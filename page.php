<?php /* Template Name: Default Page */ ?>

<!-- ADICIONA O HEADER.PHP NA PÁGINA -->
<?php include('header.php'); ?>

<!-- DEFAULT PAGE -->
<section class="default-page">    
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1">

                <!-- LOOP DO WORDPRESS -->
                <?php if (have_posts()) :
                    while (have_posts()) : the_post(); ?>
                        <h1><?php the_title(); ?></h1>
                        <?php the_content();
                    endwhile;
                endif; ?> 
                
            </div>
        </div>
    </div>
</section>

<!-- ADICIONA O FOOTER.PHP NA PÁGINA -->
<?php include(footer.php); ?>
