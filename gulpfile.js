var gulp        = require('gulp');
var	gutil       = require('gulp-util');
var plumber     = require('gulp-plumber');
var sourcemaps  = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();


// Origens
var paths = {
	style:  ['./assets/style/*.css'],
	script: ['./assets/script/**/*.js'],
	images: ['./assets/images/**/*'],
	php:    ['./**/*.php'],
	deploy: ['./**/*', '!./assets/**/*', '!./node_modules/**/*', '!./gulpfile.js', '!./package.json', '!./.gitignore', '!./.git'],
};


// Destino dos arquivos compilados
var dest = './dist/';


// css
gulp.task('css', function() {
	var postcss       = require('gulp-postcss');
	var precss        = require('precss');
	var autoprefixer  = require('autoprefixer');
	var pxtorem       = require('postcss-pxtorem');
	var colorFunction = require('postcss-color-function');
	var math          = require('postcss-math');
	var cleanCSS      = require('gulp-clean-css');

	return gulp.src(paths.style)
		.pipe(plumber())
		// .pipe(sourcemaps.init())
		.pipe(
			postcss([ 
				precss(),
				math(),
				pxtorem(),
				colorFunction(),
				autoprefixer({ browsers: ['last 10 versions', 'ie 8-9', '> 1%'] }),
			])
		)
		.pipe(cleanCSS())
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest(dest+'style/'));
});


// js
gulp.task('js', function() {
	var stripDebug = require('gulp-strip-debug');
	var uglify     = require('gulp-uglify');
	var concat     = require('gulp-concat');

	return gulp.src(paths.script)
		.pipe(plumber())
		// .pipe(sourcemaps.init())
		// .pipe(stripDebug())
		.pipe(uglify())
		// .pipe(concat('all.js'))
		// .pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(dest+'script/'));
});


// images
gulp.task('images', function() {
	var imagemin = require('gulp-imagemin');

	return gulp.src(paths.images)
		.pipe(plumber())
		.pipe(imagemin({progressive: true, optimizationLevel: 7}))
		.pipe(gulp.dest(dest+'images/'));
});


// php
gulp.task('php', function() { });


// CSS Critical
gulp.task('critical', function() {
	var critical = require('critical');

	critical.generate({
		src: 'http://www.mdsweb.com.br',
		ignore: [/url\(/],
		/* baseado nos acessos pelo analytics */
		dimensions: [{
			height: 360,
			width: 640
		}, {
			height: 1366,
			width: 768
		}],
		minify: true,
		dest: './dist/style/critical.css',
	});
});


// Sobe as coisas para o servidor
gulp.task('deploy', function () {
	var sftp = require('gulp-sftp');
	var cache = require('gulp-cached');

	return gulp.src(paths.deploy)
		.pipe(plumber())
		.pipe(cache('deploy'))
		.pipe(sftp({
			host: 'mdsweb.com.br',
			user: 'usuario',
			pass: 'senha',
			remotePath: '/home/www/site/wp-content/themes/site-theme/'
		}));
});


// Reload
gulp.task('reload', ['deploy'], function () {
	browserSync.reload();
});


// browser sync
gulp.task('serve', function() {
	browserSync.init({
		proxy: "http://www.mdsweb.com.br"
	});

	gulp.watch(paths.style,  ['css', 'reload'] );
	gulp.watch(paths.images, ['images', 'reload'] );
	gulp.watch(paths.script, ['js', 'reload'] );
	gulp.watch(paths.php,    ['php', 'reload'] );
});

// Task padrão
gulp.task('default', ['serve']);
