<!DOCTYPE html> 
<html lang="pt-br"> 
<head>
    <title>Starter Site Pack</title>
    <meta name="description" content="Descrição do site" />
    <meta name="keywords" content="palavra, chave, key, word0" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- ADICIONA UM BACKGROUND NA BARRA DO NAVEGADOR (SOMENTE MOBILE)-->
    <meta name="theme-color" content="#0052CC"> 

    <!-- STYLES -->
    <link rel="stylesheet" href="dist/style/main.css" />
    <link rel="stylesheet" href="dist/style/bootstrap.css" />    
     
    <!-- REMOVER ESSA TAG QUANDO SUBIR O SITE NO LINK FINAL -->
    <meta name="robots" content="noindex, nofollow">

    <!-- META OG PROPERTY TAGS -->
    <meta property="og:title" content="Starter Site Pack" />
    <meta property="og:description" content="Descrição do site" />
    <meta property="og:author" content="Matheus Decleve"
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:url" content="http://www.urldosite.com.br/" />
    <meta property="og:image" content="http://www.urldosite.com.br/screenshot.png" />

    <!-- FAVICONS -->
    <link rel="apple-touch-icon" sizes="180x180" href="/dist/images/favicons/apple-touch-icon.png'; ?>">
	<link rel="icon" type="image/png" href="dist/images/favicons/favicon-32x32.png" sizes="32x32'; ?>">
	<link rel="icon" type="image/png" href="dist/images/favicons/favicon-16x16.png" sizes="16x16'; ?>">
	<link rel="shortcut icon" href="dist/images/favicons/favicon.png'; ?>">
 </head> 
<body>

<!-- MENU DO SITE -->
<header>        
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark">
        <a class="navbar-brand" href="index.php">
            <img src="dist/images/logo.png" class="d-inline-block align-top" alt="Logo Gears">            
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="#!">Home</a></li>               
                <li class="nav-item"><a class="nav-link" href="#!">Quem Somos</a></li>
                <li class="nav-item"><a class="nav-link" href="#!">Contatos</a></li>                        
            </ul> 
        </div>
    </nav>
</header>